# Task3
# print all comparison operations result
# e.g
# print(5 > 9)
# print(2 != 3)
# e.t.c
print(2 < 4)
print(3 > 8)
print(7 >= 7)
print(7 >= 8)
print(7 <= 9)
print(6 <= 2)
print(5 != 9)
print(4 != 4)
print(7 == 7)
print(2 == 3)